# -*- coding:utf-8 -*-

# 編集したら, <kbd>rails routes</kbd> で確認すること.

Rails.application.routes.draw do
  root 'welcome#index'

  # 単数形リソースであっても，デフォルトコントローラ名は複数形.
  # => `controller:` でクラス名を指定.
  resource :account, only: [:show, :create, :edit, :update],
                     controller: 'account' do
    get 'sign_in'
  end

  resource :session, only: %i[create, destroy], controller: 'session'

  ####################################
  # OmniAuth mapping
  
  # POST /auth/:provider => ログイン開始

  # OmniAuth -> アプリケィション
  match 'auth/:provider/callback', via: [:get, :post], to: 'session#create'
  get 'auth/failure', to: redirect('/')

  get 'auth/:provider/implicit_back', to: 'session#implicit_back' # The Implicit Flow

  #delete 'session/sign_out', to: 'session#destroy'

  # For details on the DSL available within this file,
  # see https://guides.rubyonrails.org/routing.html
end
