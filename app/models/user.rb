# -*- coding:utf-8 -*-

# ユーザ
class User < ApplicationRecord
  validates :name, presence:true
  
  # メールで識別.
  validates :email, presence:true
  validates :email, uniqueness: true

  # @yield [user, failure_reason]
  def self.authenticate auth_hash
    raise TypeError if !auth_hash.is_a?(OmniAuth::AuthHash)
    
    user = User.find_or_initialize_by(email: auth_hash.info[:email])
    user.name  = auth_hash.info[:name]
    user.image = auth_hash.info[:image]
    user.save!

    if block_given?
      yield user, nil
    end
    return user
  end
  
=begin
Google: 
#<OmniAuth::AuthHash 
    credentials=#<OmniAuth::AuthHash 略> 
    extra=#<OmniAuth::AuthHash 
        raw_info=#<OmniAuth::AuthHash 
            email="hisashi.horikawa@gmail.com" email_verified=true family_name="Horikawa" given_name="Hisashi" locale="ja" name="Hisashi Horikawa" picture="https://lh3.googleusercontent.com/a-/AOh14GjV5VAFR3FZxvJbQQwahq-StqNyl2qYR1iSjI_x=s96-c" 
            sub="略"> > 
    info=#<OmniAuth::AuthHash::InfoHash 
        email="hisashi.horikawa@gmail.com" 
        first_name="Hisashi" gender=nil 
        image="https://lh3.googleusercontent.com/a-/AOh14GjV5VAFR3FZxvJbQQwahq-StqNyl2qYR1iSjI_x=s96-c" last_name="Horikawa" 
        name="Hisashi Horikawa" nickname=nil phone=nil 
        urls=#<OmniAuth::AuthHash website=nil>> 
    provider="google_codeflow" 
    uid="略">

Azure AD v2:
#<OmniAuth::AuthHash 
    credentials=#<OmniAuth::AuthHash 略>
    extra=#<OmniAuth::AuthHash 
        raw_info=#<OmniAuth::AuthHash 
            family_name="ほりかわ" given_name="ひさし" name="ほりかわ ひさし" picture="https://graph.microsoft.com/v1.0/me/photo/$value" sub="zwZ3FwLWw6-viAhaGntG70JtuMT61I8vwLO4KMiNtJs">> info=#<OmniAuth::AuthHash::InfoHash email=nil first_name="ひさし" gender=nil image="https://graph.microsoft.com/v1.0/me/photo/$value" last_name="ほりかわ" name="ほりかわ ひさし" nickname=nil phone=nil urls=#<OmniAuth::AuthHash website=nil>> 
    provider="azure_ad_codeflow" 
    uid="略">
>>  

=end    

end # class User
