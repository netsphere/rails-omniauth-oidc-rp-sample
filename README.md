# Recommend omitting OmniAuth
The OmniAuth library is still maintained. You always use it with some authentication framework. However, if you combine it with the OpenId Connect library, the both libraries handle multi-providers. That's gilding the lily.

Therefore, it is good to combine an authentication framework, which the good one is Sorcery, directly with the OpenId Connect library without using OmniAuth.



# Rails, OmniAuth2, OpenID Connect sample

omniauth-openid-connect のRails サンプルプログラム. See https://www.nslabs.jp/omniauth-openid-connect.rhtml

Gem packages:
 - rails, ~> 6.1.3
 - omniauth, ~> 2.0
 - openid_connect, ~> 1.2.0
 - omniauth_openid_connect, >= 0.4.0.pre

`omniauth_openid_connect` must be from https://github.com/omniauth/omniauth_openid_connect/

OmniAuthは外部IdPで認証するためのクライアント側フレームワーク。通常は Devise などの認証ライブラリと組み合わせる。しかし、外部IdPのみしか使わないなら、単独で利用することもできる。これはそのサンプル。


## Features

(1) OpenID Connect Authorization Code Flow によるログイン

Tested Google, Yahoo! JAPAN, Azure AD, and Nov OP Sample.

このサンプルは, Just-in-time User Provisioning (JIT Provisioning) の例にもなっている。あらかじめユーザ登録されていなくても、シングルサインオンで回ってくるユーザ情報で自サイトにも登録する。


(2) Implicit Flow によるログイン

`omniauth_openid_connect` v0.4.0 は, <code>response_type=id_token</code> しかサポートしていない。IdP が発行する <code>id_token</code> にユーザ情報を含める拡張を行っている場合のみ、利用可能。Yahoo! JAPAN ID連携は、ユーザ情報を含めないので、動作しない。

Azure AD は、管理画面で Implicit Flow を有効にしている場合のみ。



(3) OpenID Connect RP-Initiated Logout 1.0 によるシングルログアウト (SLO)

IdP が <code>end_session_endpoint</code> をサポートしている場合、利用可能。
Azure AD v2 は、主に企業ユースで、シングルログアウトを必要とするユースケースがあるので、サポートしている。

シングルログアウトは, OpenID Connect (や他の OAuth2 上に作られたプロファイル) を使ったユースケースでは, 必要にならないことが多い。各サービス (Relying Party 側) がログアウトするときに Google とかもログアウトしたら、逆に困る。




## Usage

`config/database.yml.sample` を `database.yml` にコピーし, 適宜修正.

<pre>
  $ <kbd>rake db:migrate</kbd>
  $ <kbd>bin/yarn</kbd>
</pre>

次のような起動スクリプトを作って, 実行.

```sh
#!/bin/sh

export GOOGLE_CLIENT_ID='..........'
export GOOGLE_CLIENT_SECRET='.......'

export MYSAMPLE_HOST='http://localhost:4000'
passenger start -a localhost -p 4000
```

リダイレクトURI は次のとおり;
`http://localhost:4000/auth/google_codeflow/callback`
`http://localhost:4000/auth/google_implicit/implicit_back`



## 先行事例

[Rails+omniauth-google-oauth2でGoogleログイン(devise無し)](https://zenn.dev/batacon/articles/e9b4a88ede2889)

 - omniauth-google-oauth2 のような IdP ごとに別パッケージを使うのではなく, OpenID Connect で統一的にシングルサインオンしたほうが、穴を作らない、と言う意味でもいい。
 - Devise を使わないのはいいが、自作のログインコード、ログアウトコードが雑すぎて、これを参考にするのはむしろ有害。こういう雑なサンプルは、止めてほしい。



