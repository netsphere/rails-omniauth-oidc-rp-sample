# -*- coding:utf-8 -*-

# $ rails g model User name email image
# 適宜修正
# $ rails db:migrate

# ユーザ
class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :name,  null:false
      t.string :email, null:false
      t.string :image
      t.timestamps  null:false
    end
    add_index :users, [:email], unique:true
  end
end
