# -*- coding:utf-8 -*-

# ベース
class ApplicationController < ActionController::Base
  # コントローラのメソッドを view でも呼び出せるようにする
  helper_method :current_user
  helper_method :logged_in?

  
private

  def current_user
    if !@current_user
      @current_user = login_from_session()
    end
    @current_user
  end

  def logged_in?
    !!current_user
  end
  
  def login_from_session
    @current_user = if session[:user_id]
                      User.find_by_id(session[:user_id])
                    end
  end

  # For `before_action`
  def require_login
    return if logged_in?
    session[:return_to_url] = request.url
    # TODO: flash
    redirect_to 'account#sign_in'
  end

  # @yield [user, failure_reason]
  # @return current_user or nil.
  # 地味に大変.
  def login auth_hash
    raise TypeError if !auth_hash

    @current_user = nil
    User.authenticate(auth_hash) do |user, failure_reason|
      if failure_reason
        yield user, failure_reason if block_given?
        return nil
      end

      # session id のみ差し替え. 重要!
      old_session = session.dup.to_hash
      reset_session()
      old_session.each_pair do |k, v|
        session[k.to_sym] = v
      end
      form_authenticity_token() # CSRF Protection in Rails

      # ユーザ設定
      raise TypeError if !user.is_a?(User)
      session[:user_id] = user.id.to_s
      @current_user = user

      yield user, nil if block_given?
      return current_user
    end
  end
  
  def logout
    return unless logged_in?

    user = current_user
    @current_user = nil
    reset_session() # protect from session fixation attacks
  end
end
